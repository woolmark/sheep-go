//
//  SheepView.h
//  SheepMacOS
//
//  Created by Naoki Takimura on 8/1/16.
//  Copyright © 2016 Naoki Takimura. All rights reserved.
//

#ifndef SheepView_h
#define SheepView_h

#include <stdint.h>
typedef uintptr_t SGSheepView;

typedef void (* SGSheepViewDrawHandler)(const SGSheepView view);

const SGSheepView SGNewSheepView();

void SGSheepViewSetNeedsDisplay(const SGSheepView view, int display );

// MARK: Properties

void SGSheepViewSetSize(const SGSheepView view, int width, int height);
void SGSheepViewSetDrawHandler(const SGSheepView view, SGSheepViewDrawHandler handler);

// MARK: MUST call in draw handler

void SGSheepViewSetColor(const SGSheepView ptrView, int r, int g, int b);
void SGSheepViewFillRect(const SGSheepView ptrView, int x, int y, int width, int height);
void SGSheepViewDrawCount(const SGSheepView view, int count, int x, int y);
void SGSheepViewDrawSheep(const SGSheepView view, int x, int y, int width, int height, int stretch);
void SGSheepViewDrawFence(const SGSheepView view, int x, int y, int width, int height);

#endif /* SheepView_h */
