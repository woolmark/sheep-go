//
//  Application.h
//  SheepMacOS
//
//  Created by Naoki Takimura on 8/1/16.
//  Copyright © 2016 Naoki Takimura. All rights reserved.
//

#ifndef Application_h
#define Application_h

#include <stdint.h>
typedef uintptr_t SGApplication;

const SGApplication SGSharedApplication();

void SGReleaseApplication(const SGApplication app);

void SGRunApplication(const SGApplication app);

void SGStopApplication(const SGApplication app);

#endif /* Application_h */
