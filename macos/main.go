package main

import (
	wm "bitbucket.org/woolmark/sheep-go"
	"fmt"
	"sync"
	"time"
)

var (
	frame wm.Frame
	count int
	flock []wm.Sheep
	mutex *sync.Mutex
)

func onDraw(view *SheepView) {
	mutex.Lock()
	defer mutex.Unlock()

	view.SetColor(150, 150, 255)
	view.FillRect(frame.X(), frame.Y(), frame.Width(), frame.Height())

	view.SetColor(100, 255, 100)
	view.FillRect(frame.Ground.X(), frame.Ground.Y(), frame.Ground.Width(), frame.Ground.Height())

	view.DrawFence(frame.Fence.X(), frame.Fence.Y(), frame.Fence.Width(), frame.Fence.Height())

	for _, sheep := range flock {
		view.DrawSheep(sheep.X(), sheep.Y(), sheep.Width(), sheep.Height(), sheep.Stretch())
	}

	view.DrawCount(count, 5, 5)

}

func update(view *SheepView, touched bool) {
	mutex.Lock()
	defer mutex.Unlock()

	newFlock := make([]wm.Sheep, 0)

	for _, sheep := range flock {
		sheep = wm.MoveSheep(sheep)
		if sheep.MaximalPosition() {
			count++
		}
		if !sheep.GoneAway() {
			newFlock = append(newFlock, sheep)
		}
	}
	if len(newFlock) <= 0 || touched {
		newFlock = append(newFlock, wm.NewSheep(frame))
	}

	flock = newFlock

	view.SetNeedsDisplay(true)
}

func main() {

	mutex = new(sync.Mutex)

	ch := make(chan bool)

	frame = wm.NewFrame(120, 120)

	app := SharedApplication()
	window := NewWindow(frame.Width(), frame.Height())
	view := NewSheepView()

	view.SetDrawHandler(onDraw)
	view.SetSize(frame.Width(), frame.Height())
	view.SetTouchChannel(ch)

	window.SetTitle("sheep")
	window.Center()
	window.AddView(view)
	window.MakeKeyAndOrderFront()

	touched := false
	go func() {
		for {
			switch <-ch {
			case true:
				touched = true
			case false:
				touched = false
			default:
			}
		}
	}()

	go func() {
		for {
			update(view, touched)
			time.Sleep(100 * time.Millisecond)
		}
	}()

	fmt.Println(app.Run())

}
