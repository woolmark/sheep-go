//
//  Application.m
//  SheepMacOS
//
//  Created by Naoki Takimura on 8/1/16.
//  Copyright © 2016 Naoki Takimura. All rights reserved.
//

#import "Application.h"
#import <Cocoa/Cocoa.h>

const SGApplication SGSharedApplication()
{
    @autoreleasepool {
        return (SGApplication) CFBridgingRetain([NSApplication sharedApplication]);
    }
}

void SGRunApplication(const SGApplication ptrApp)
{
    NSApplication *app = (__bridge NSApplication *)(void *)ptrApp;
    @autoreleasepool {
        [app run];
    }
}

void SGStopApplication(const SGApplication ptrApp)
{
    NSApplication *app = (__bridge NSApplication *)(void *)ptrApp;
    @autoreleasepool {
        [app stop:nil];
    }
}
