//
//  Window.m
//  SheepMacOS
//
//  Created by Naoki Takimura on 8/1/16.
//  Copyright © 2016 Naoki Takimura. All rights reserved.
//

#import "Window.h"
#import "Application.h"
#import <Cocoa/Cocoa.h>

#ifdef AVAILABLE_MAC_OS_X_VERSION_10_12_AND_LATER
#define SGWindowStyle (NSWindowStyleMaskTitled | NSWindowStyleMaskClosable)
#else
#define SGWindowStyle (NSTitledWindowMask | NSClosableWindowMask)
#endif

const SGWindow SGNewWindow(int width, int height)
{
    @autoreleasepool {
        NSWindow *window = [[NSWindow alloc] initWithContentRect:NSMakeRect(0, 0, width, height) styleMask:SGWindowStyle backing:NSBackingStoreBuffered defer:NO];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:NSWindowWillCloseNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            SGStopApplication(SGSharedApplication());
        }];
        
        return (SGWindow) CFBridgingRetain(window);
    }
}

void SGWindowMakeKeyAndOrderFront(const SGWindow ptrWindow)
{
    NSWindow *window = (__bridge NSWindow *)(void *)ptrWindow;
    @autoreleasepool {
        [window makeKeyAndOrderFront:nil];
    }
}

void SGWindowSetTitle(const SGWindow ptrWindow, char* title)
{
    NSWindow *window = (__bridge NSWindow *)(void *)ptrWindow;
    @autoreleasepool {
        [window setTitle:[NSString stringWithUTF8String:title]];
    }
}

void SGWindowCenter(const SGWindow ptrWindow)
{
    NSWindow *window = (__bridge NSWindow *)(void *)ptrWindow;
    @autoreleasepool {
        [window center];
    }
}

void SGWindowAddView(const SGWindow ptrWindow, const SGSheepView view)
{
    NSWindow *window = (__bridge NSWindow *)(void *)ptrWindow;
    @autoreleasepool {
        [window.contentView addSubview:(__bridge NSView *)(void *)view];
    }
}
