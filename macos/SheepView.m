//
//  SheepView.m
//  SheepMacOS
//
//  Created by Naoki Takimura on 8/1/16.
//  Copyright © 2016 Naoki Takimura. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SheepView.h"
#import "sheep_img.h"

#ifdef AVAILABLE_MAC_OS_X_VERSION_10_12_AND_LATER
#define SGCompositingOperationSourceOver NSCompositingOperationSourceOver
#else
#define SGCompositingOperationSourceOver NSCompositeSourceOver
#endif

#ifdef CGO
extern void SheepViewOnDraw(const SGSheepView view);
extern void SheepViewOnTouchStart(const SGSheepView view);
extern void SheepViewOnTouchEnd(const SGSheepView view);
#endif

@interface SheepView : NSView

@property (retain, nonatomic) NSImage *imageSheep00;
@property (retain, nonatomic) NSImage *imageSheep01;
@property (retain, nonatomic) NSImage *imageFence;
@property (retain, nonatomic) NSColor *color;

@property (nonatomic) SGSheepViewDrawHandler DrawHandler;

- (void)fillRect:(NSRect)rect;
- (void)drawFenceAt:(NSRect)rect;
- (void)drawSheepAt:(NSRect)rect stretch:(BOOL)stretch;
- (void)drawCount:(int)count at:(NSPoint)point;

@end

const SGSheepView SGNewSheepView()
{
    @autoreleasepool {
        SheepView *view = [[SheepView alloc] init];
        return (SGSheepView) CFBridgingRetain(view);
    }
}

void SGSheepViewSetNeedsDisplay(const SGSheepView ptrView, int display)
{
    SheepView *view = ((__bridge SheepView *)(void *)ptrView);
    @autoreleasepool {
        [view setNeedsDisplay:(display == 1) ? YES : NO];
    }
}

void SGSheepViewSetDrawHandler(const SGSheepView ptrView, SGSheepViewDrawHandler handler)
{
    SheepView *view = ((__bridge SheepView *)(void *)ptrView);
    @autoreleasepool {
        view.DrawHandler = handler;
    }
}

void SGSheepViewSetSize(const SGSheepView ptrView, int width, int height)
{
    SheepView *view = ((__bridge SheepView *)(void *)ptrView);
    @autoreleasepool {
        view.frame = NSMakeRect(0, 0, width, height);
    }
}

void SGSheepViewSetColor(const SGSheepView ptrView, int r, int g, int b)
{
    SheepView *view = ((__bridge SheepView *)(void *)ptrView);
    @autoreleasepool {
        view.color = [NSColor colorWithRed:((CGFloat)r / 255) green:((CGFloat)g / 255) blue:((CGFloat)b / 255) alpha:1];
    }
}

void SGSheepViewFillRect(const SGSheepView ptrView, int x, int y, int width, int height)
{
    SheepView *view = ((__bridge SheepView *)(void *)ptrView);
    @autoreleasepool {
        [view fillRect:NSMakeRect(x, view.frame.size.height - y - height, width, height)];
    }
}

void SGSheepViewDrawCount(const SGSheepView ptrView, int count, int x, int y)
{
    SheepView *view = ((__bridge SheepView *)(void *)ptrView);
    @autoreleasepool {
        [view drawCount:count at:NSMakePoint(x, view.frame.size.height - y - 12)];
    }
}

void SGSheepViewDrawSheep(const SGSheepView ptrView, int x, int y, int width, int height, int stretch)
{
    SheepView *view = ((__bridge SheepView *)(void *)ptrView);
    @autoreleasepool {
        [view drawSheepAt:NSMakeRect(x, view.frame.size.height - y - height, width, height) stretch:stretch];
    }
}

void SGSheepViewDrawFence(const SGSheepView ptrView, int x, int y, int width, int height)
{
    SheepView *view = ((__bridge SheepView *)(void *)ptrView);
    @autoreleasepool {
        [view drawFenceAt:NSMakeRect(x, view.frame.size.height - y - height, width, height)];
    }
}

@implementation SheepView

- (id)init
{
    self = [super init];
    if (self) {
        self.imageSheep00 = [[NSImage alloc] initWithData:[[NSData alloc] initWithBase64EncodedData:[NSData dataWithBytes:SHEEP00_PNG_BASE64 length:strlen(SHEEP00_PNG_BASE64)] options:0]];
        self.imageSheep01 = [[NSImage alloc] initWithData:[[NSData alloc] initWithBase64EncodedData:[NSData dataWithBytes:SHEEP01_PNG_BASE64 length:strlen(SHEEP01_PNG_BASE64)] options:0]];
        self.imageFence = [[NSImage alloc] initWithData:[[NSData alloc] initWithBase64EncodedData:[NSData dataWithBytes:FENCE_PNG_BASE64 length:strlen(FENCE_PNG_BASE64)] options:0]];
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    if (self.DrawHandler) {
        self.DrawHandler((const SGSheepView)(self));
    }

#ifdef CGO
    SheepViewOnDraw((const SGSheepView)self);
#endif
}

- (void)mouseDown:(NSEvent *)event
{
#ifdef CGO
    SheepViewOnTouchStart((const SGSheepView)self);
#endif
}

- (void)mouseUp:(NSEvent *)event
{
#ifdef CGO
    SheepViewOnTouchEnd((const SGSheepView)self);
#endif
}

- (void)setColor:(NSColor *)color
{
    [color set];
}

- (void)fillRect:(NSRect)rect
{
    NSRectFill(rect);
}

- (void)drawFenceAt:(NSRect)rect
{
    [self.imageFence drawAtPoint:rect.origin fromRect:NSZeroRect operation:SGCompositingOperationSourceOver fraction:1];
}

- (void)drawSheepAt:(NSRect)rect stretch:(BOOL)stretch
{
    if (stretch) {
        [self.imageSheep01 drawAtPoint:rect.origin fromRect:NSZeroRect operation:SGCompositingOperationSourceOver fraction:1];
        
    } else {
        [self.imageSheep00 drawAtPoint:rect.origin fromRect:NSZeroRect operation:SGCompositingOperationSourceOver fraction:1];
    }
}

- (void)drawCount:(int)count at:(NSPoint)point
{
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d sheep", count]];
    [string drawAtPoint:point];    
}

@end
