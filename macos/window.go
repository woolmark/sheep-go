package main

/*
#cgo CFLAGS: -x objective-c
#cgo LDFLAGS: -framework Cocoa
#include "window.h"
*/
import "C"

type Window struct {
	ptr C.SGWindow
}

func NewWindow(width int, height int) *Window {
	window := new(Window)
	window.ptr = C.SGNewWindow(C.int(width), C.int(height))

	return window
}

func (window *Window) MakeKeyAndOrderFront() {
	C.SGWindowMakeKeyAndOrderFront(window.ptr)
}

func (window *Window) SetTitle(title string) {
	C.SGWindowSetTitle(window.ptr, C.CString(title))
}

func (window *Window) Center() {
	C.SGWindowCenter(window.ptr)
}

func (window *Window) AddView(view *SheepView) {
	C.SGWindowAddView(window.ptr, view.ptr)
}
