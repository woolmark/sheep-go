//
//  Window.h
//  SheepMacOS
//
//  Created by Naoki Takimura on 8/1/16.
//  Copyright © 2016 Naoki Takimura. All rights reserved.
//

#ifndef Window_h
#define Window_h

#include "SheepView.h"

#include <stdint.h>
typedef uintptr_t SGWindow;

const SGWindow SGNewWindow(int width, int height);

void SGReleaseWindow(const SGWindow window);

void SGWindowMakeKeyAndOrderFront(const SGWindow window);

void SGWindowSetTitle(const SGWindow window, char* title);

void SGWindowCenter(const SGWindow window);

void SGWindowAddView(const SGWindow window, const SGSheepView view);

#endif /* Window_h */
