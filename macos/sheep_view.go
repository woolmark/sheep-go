package main

/*
#cgo CFLAGS: -x objective-c -DCGO
#cgo LDFLAGS: -framework Cocoa
#include "SheepView.h"
*/
import "C"

type SheepView struct {
	ptr C.SGSheepView
}

var drawHandler func(*SheepView)
var chTouch chan bool

func NewSheepView() *SheepView {
	return newSheepView(C.SGNewSheepView())
}

func newSheepView(ptr C.SGSheepView) *SheepView {
	view := new(SheepView)
	view.ptr = ptr

	return view
}

func (view *SheepView) SetNeedsDisplay(display bool) {
	if display {
		C.SGSheepViewSetNeedsDisplay(view.ptr, 1)
	} else {
		C.SGSheepViewSetNeedsDisplay(view.ptr, 0)
	}
}

func (view *SheepView) SetSize(width int, height int) {
	C.SGSheepViewSetSize(view.ptr, C.int(width), C.int(height))
}

func (view *SheepView) SetDrawHandler(handler func(*SheepView)) {
	drawHandler = handler
}

func (view *SheepView) SetTouchChannel(ch chan bool) {
	chTouch = ch
}

//export SheepViewOnDraw
func SheepViewOnDraw(ptr C.SGSheepView) {
	if drawHandler != nil {
		drawHandler(newSheepView(ptr))
	}
}

//export SheepViewOnTouchStart
func SheepViewOnTouchStart(ptr C.SGSheepView) {
	if chTouch != nil {
		chTouch <- true
	}
}

//export SheepViewOnTouchEnd
func SheepViewOnTouchEnd(ptr C.SGSheepView) {
	if chTouch != nil {
		chTouch <- false
	}
}

func (view *SheepView) SetColor(r int, g int, b int) {
	C.SGSheepViewSetColor(view.ptr, C.int(r), C.int(g), C.int(b))
}

func (view *SheepView) FillRect(x int, y int, width int, height int) {
	C.SGSheepViewFillRect(view.ptr, C.int(x), C.int(y), C.int(width), C.int(height))
}

func (view *SheepView) DrawCount(count int, x int, y int) {
	C.SGSheepViewDrawCount(view.ptr, C.int(count), C.int(x), C.int(y))
}

func (view *SheepView) DrawSheep(x int, y int, width int, height int, stretch bool) {
	if stretch {
		C.SGSheepViewDrawSheep(view.ptr, C.int(x), C.int(y), C.int(width), C.int(height), 1)
	} else {
		C.SGSheepViewDrawSheep(view.ptr, C.int(x), C.int(y), C.int(width), C.int(height), 0)
	}
}

func (view *SheepView) DrawFence(x int, y int, width int, height int) {
	C.SGSheepViewDrawFence(view.ptr, C.int(x), C.int(y), C.int(width), C.int(height))
}
