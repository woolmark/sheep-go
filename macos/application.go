package main

/*
#cgo CFLAGS: -x objective-c
#cgo LDFLAGS: -framework Cocoa
#include "application.h"
*/
import "C"

var app *Application

type Application struct {
	ptr C.SGApplication
}

func SharedApplication() *Application {
	if app == nil {
		app = new(Application)
	}
	if app.ptr == 0 {
		app.ptr = C.SGSharedApplication()
	}

	return app
}

func (app *Application) Run() bool {
	if app != nil && app.ptr != 0 {
		C.SGRunApplication(app.ptr)
		return true
	}
	return false
}

func (app *Application) Stop() bool {
	if app != nil && app.ptr != 0 {
		C.SGStopApplication(app.ptr)
		return true
	}
	return false
}
