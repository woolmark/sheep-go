package main

import (
	wm "bitbucket.org/woolmark/sheep-go"
	"fmt"
)

func main() {
	frame := wm.NewFrame(120, 120)
	sheep := wm.NewSheep(frame)

	fmt.Println(frame)

	chMove := make(chan wm.Sheep)
	chJump := make(chan wm.Sheep)

	go wm.RunSheep(sheep, chMove, chJump)

run:
	for {
		select {
		case sheep, running := <-chMove:
			if !running {
				break run
			}
			fmt.Println(sheep)
		case <-chJump:
			fmt.Println("JUMP!!")
		}
	}

}
