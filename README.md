# sheep for Go
sheep for Go is an implementation [sheep] with [Go].

    % git clone git@bitbucket.org:woolmark/sheep-go.git $GOPATH/src/bitbucket.org/woolmark/sheep-go

## CLI(Command Line Interface)

    % cd $GOPATH/src/bitbucket.org/woolmark/sheep-go/cli
    % go build
    % ./cli

or

    % go run $GOPATH/src/bitbucket.org/woolmark/sheep-go/cli/main.go

## macOS with Cocoa

    % cd $GOPATH/src/bitbucket.org/woolmark/sheep-go/macos
    % go build
    % ./macos

# License
[WTFPL]

[sheep]: https://woolmark.bitbucket.org/ "Sheep"
[Go]: https://golang.org "Go"
[WTFPL]: http://www.wtfpl.net "WTFPL"

