package sheep

import (
	"fmt"
	"math/rand"
	"time"
)

const (
	DefaultFrameWidth  = 120
	DefaultFrameHeight = 120

	SheepWidth  = 17
	SheepHeight = 12

	FenceWidth  = 52
	FenceHeight = 78

	sheepMoveX     = 5
	sheepMoveY     = 3
	sheepJumpFrame = 4
)

type Rect struct {
	x      int
	y      int
	width  int
	height int
}

func (rect Rect) X() int {
	return rect.x
}

func (rect Rect) Y() int {
	return rect.y
}

func (rect Rect) Width() int {
	return rect.width
}

func (rect Rect) Height() int {
	return rect.height
}

func (rect Rect) String() string {
	return fmt.Sprintf("{%d, %d, %d, %d}", rect.x, rect.y, rect.width, rect.height)
}

type Sheep struct {
	Rect
	stretch   bool
	scale     int
	jumpX     int
	jumpState int
}

func NewSheep(frame Frame) Sheep {

	sheep := Sheep{}

	sheep.width = SheepWidth * frame.scale
	sheep.height = SheepHeight * frame.scale
	sheep.x = frame.width
	sheep.y = frame.height - frame.Ground.height + rand.New(rand.NewSource(time.Now().UnixNano())).Intn(frame.Ground.height-sheep.height)

	sheep.jumpX = -1*(sheep.y-frame.height)*frame.Fence.width/frame.Fence.height + (frame.width-frame.Fence.width)/2
	sheep.jumpState = -1

	sheep.scale = frame.scale

	return sheep
}

func (sheep Sheep) String() string {
	return fmt.Sprintf("(%d, %d) stretch:%t", sheep.x, sheep.y, sheep.stretch)
}

func (sheep Sheep) GoneAway() bool {
	return sheep.x < -sheep.width
}

func (sheep Sheep) MaximalPosition() bool {
	return sheep.jumpState == sheepJumpFrame
}

func (sheep Sheep) Stretch() bool {
	return sheep.stretch
}

func (sheep Sheep) Scale() int {
	return sheep.scale
}

func RunSheep(sheep Sheep, chMove chan Sheep, chJump chan Sheep) {
	for !sheep.GoneAway() {
		sheep = MoveSheep(sheep)
		chMove <- sheep

		if sheep.MaximalPosition() {
			chJump <- sheep
		}
		time.Sleep(100 * time.Millisecond)
	}
	close(chMove)
}

func MoveSheep(sheep Sheep) Sheep {

	sheep.x -= sheepMoveX * sheep.scale

	if sheep.jumpState < 0 && sheep.jumpX <= sheep.x && sheep.x < sheep.jumpX+sheepMoveX*sheep.scale {
		sheep.jumpState = 0
	}

	// fmt.Println(sheep.jumpState)
	switch {
	case sheep.jumpState < 0:
		sheep.stretch = !sheep.stretch

	case sheep.jumpState >= sheepJumpFrame*2:
		sheep.jumpState = -1

	case sheep.jumpState < sheepJumpFrame:
		sheep.y -= sheepMoveY * sheep.scale
		sheep.jumpState++
		sheep.stretch = true

	case sheep.jumpState < sheepJumpFrame*2:
		sheep.y += sheepMoveY * sheep.scale
		sheep.jumpState++
		sheep.stretch = true
	}

	return sheep

}

type Frame struct {
	Rect
	Ground Rect
	Fence  Rect
	scale  int
}

func NewFrame(width int, height int) Frame {
	scale := 0
	if width < height {
		scale = int(width / DefaultFrameWidth)
	} else {
		scale = int(height / DefaultFrameHeight)
	}

	if scale < 1 {
		scale = 1
	}

	fw := FenceWidth * scale
	fh := FenceHeight * scale
	gh := int(float32(FenceHeight*scale) * 0.9)

	return Frame{
		Rect: Rect{
			x:      0,
			y:      0,
			width:  width,
			height: height,
		},
		Fence: Rect{
			x:      (width - fw) / 2,
			y:      height - fh,
			width:  fw,
			height: fh,
		},
		Ground: Rect{
			x:      0,
			y:      height - gh,
			width:  width,
			height: gh,
		},
		scale: scale,
	}
}

func (frame Frame) String() string {
	return fmt.Sprintf("{%d, %d, %d, %d}", frame.x, frame.y, frame.width, frame.height)
}

func (frame Frame) Scale() int {
	return frame.scale
}
