package sheep

import (
	"testing"
)

func TestSheep(t *testing.T) {

	sheep := Sheep{}

	if sheep.X() != 0 {
		t.Error("X is not 0")
	}

}

func TestRunSheep(t *testing.T) {
	sheep := Sheep{}

	w := sheep.width
	h := sheep.height

	if w != sheep.Width() {
		t.Error("width is chnaged. original:%d actual:%d", w, sheep.Width())
	}
	if h != sheep.Height() {
		t.Error("height is chnaged. original:%d actual:%d", h, sheep.Height())
	}
}

func TestFrame(t *testing.T) {
	_ = Frame{}
}

func TestFrameScale1(t *testing.T) {
	frame := NewFrame(120, 120)
	if frame.Scale() != 1 {
		t.Error("Invalid scale:", frame.Scale())
	}
}

func TestFrameScale2(t *testing.T) {
	frame := NewFrame(240, 240)
	if frame.Scale() != 2 {
		t.Error("Invalid scale:", frame.Scale())
	}
}

func TestNewSheep(t *testing.T) {
	frame := NewFrame(120, 120)
	sheep := NewSheep(frame)

	if sheep.X() < 120 {
		t.Error("Invalid x:", sheep.X())
	}

	if sheep.Y() < 0 || sheep.Y() > 120 {
		t.Error("Invalid y:", sheep.Y())
	}

	if sheep.Stretch() {
		t.Error("Invalid stretch:", sheep.Stretch())
	}
}

func TestMove1(t *testing.T) {
	frame := NewFrame(120, 120)
	sheep := NewSheep(frame)

	sheep.Move()

	if sheep.X() >= 120 || sheep.X() < 0 {
		t.Error("Invalid x:", sheep.X())
	}

	if sheep.Y() < 0 || sheep.Y() > 120 {
		t.Error("Invalid y:", sheep.Y())
	}

	if !sheep.Stretch() {
		t.Error("Invalid stretch:", sheep.Stretch())
	}
}
